THREADS=`cat *cpu* | egrep "(S|R) *[0-9]{2}" | awk '/java/ {print $1}' | sort | uniq -c | sort -r | sed s/^' '*/''/g`;
echo "CPU Hog threads have been written to cpuhogs.log";
echo "$THREADS" > cpuhogs.log;
