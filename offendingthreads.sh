echo -e "Offending Thread report:\n\n" | tee offendingthreads.log
cut -f 2 -d ' ' cpuhogs.log > thread.tmp;
( echo "obase=16" ; cat thread.tmp ) | bc > hexthreads.tmp;
cat hexthreads.tmp | while read LINE; do
	echo -e "\n########## Thread: $LINE ##########" | tee -a offendingthreads.log
	grep -i -h -A 12 $LINE *dump* | tee -a offendingthreads.log
done
rm hexthreads.tmp
rm thread.tmp
