#!/bin/bash

#find the JIRA PID
JIRA_PID=`ps aux | grep -i jira | grep -i java | awk  -F '[ ]*' '{print $2}'`;
echo "Found JIRA PID: $JIRA_PID";
echo "This process will take at least 100 seconds";
#Scrape JIRA for info
COUNTER=0;
RUNS=10;
while [  $COUNTER -lt $RUNS  ]; do
	DATE=`date +%s`
#Get us info on CPU usage per thread
	top -b -H -p $JIRA_PID -n 1 > thread_cpu_usage.$DATE.txt
#Get us thread dumps	
	jstack $JIRA_PID > thread_dump.$DATE.txt
	let COUNTER=COUNTER+1
	echo "Completed run $COUNTER of $RUNS" 
	sleep 10
done
DATE=`date +%s`;
ls *dump* -1 | xargs cat >> combined_threads.$DATE.log;
