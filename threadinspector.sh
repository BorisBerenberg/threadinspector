#!/bin/bash

#Collect Info:
/bin/bash ./getinfo.sh;

#Find out which threads are hogging the CPU
/bin/bash ./cpuhog.sh;

#Point out which threads are causing the issue:
/bin/bash ./offendingthreads.sh
