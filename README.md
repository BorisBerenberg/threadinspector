#Usage:

1. Checkout the repo
1. Identify the user JIRA runs as
1. Make sure you can _sudo_ to this user, and that the user has permission to write to the current directory
1. Run the command sudo -u <JIRA USER> threadinspector.sh (100 seconds minimum to complete)
1. Analyze the output in the offendingthreads.log file

